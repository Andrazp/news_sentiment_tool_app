import requests

def query_model(text, url):
    data = {"text": [text]}
    r = requests.get(url + "rest_api/health/")
    print(r.status_code)
    print(data)
    #headers = {'Content-Type': 'application/json; charset=utf-8'}
    r = requests.post(url + "rest_api/analyze_sentiment/", json=data)
    print(r.status_code)
    print(r.headers)
    print(r.json())

if __name__ == "__main__":
    query_model("Zaradi vojne v Ukrajini in odločitev Rusije v številnih državah strah pred lakoto\nRusija je prejšnji teden oznanila prepoved izvoza žita\nMedtem ko v Ukrajini, žitnici sveta, po napadu Rusije divjajo spopadi, Rusija pa je prejšnji teden napovedala prepoved izvoza žita, so v številnih državah zaskrbljeni zaradi pomanjkanja hrane. Cene pšenice in goriva so močno narasle.\nNemški portal Deutsche Welle (DW) poroča, da kmetje po svetu sicer pridelujejo dovolj pšenice, da nahranijo vse ljudi, težava pa je v tem, da je pšenica vse dražja, ne pridelujejo pa je tam, kjer je najbolj potrebna. Cene so spet narasle, potem ko je Rusija prejšnji teden oznanila prepoved izvoza žit, kot so pšenica, ječmen, rž in druge. Prepoved naj bi veljala najmanj do konca junija. Uradni podatki Nemčije pravijo, da ukrajinska pridelava pšenice predstavlja 11,5 odstotka svetovnega trga, ruski delež pa je 16,8-odstoten. Glede koruze Ukrajina zagotavlja 17 odstotkov svetovnega izvoza. Številne države v Afriki in na Bližnjem vzhodu so odvisne od žit iz Rusije in Ukrajine, to pa velja tudi za države, kot so Turčija, Indija in Kitajska. Afriške države so omejene v možnostih zamenjave uvoza iz Rusije in Ukrajine z medafriško trgovino, saj so regionalne zaloge pšenice relativno majhne, v številnih delih celine pa primanjkuje učinkovite prometne infrastrukture in prostora za skladiščenje, ugotavlja poročilo ZN-a o učinku vojne v Ukrajini na trgovino in razvoj, objavljeno 16. marca. Poročilo določa več dejavnikov, ki prispevajo k skrb vzbujajočemu scenariju. Vse dražja žita, energenti, gnojila in prevoz, prazni silosi in pandemija covida-19, vse to ogroža zaloge hrane za milijone ljudi in zvišuje inflacijo. Skladišča z žitom so trenutno v EU-ju dobro založena, poroča DW, kriza s hrano pa prizadeva države, ki so že imele gospodarske težave. Nekatere države preprosto nimajo več zalog hrane. Zahodnoafriške države, kot sta Senegal in Slonokoščena obala, so uvedle omejitev cen izdelkov, recimo palmovega olja, sladkorja, mleka, riža, govedine. Senegal je uvedel tudi dodatne subvencije za lokalne pridelovalce riža.",
    "http://localhost/")