## Flask REST API for cross-lingual news sentiment analysis


This repository contains a docker for cross-lingual news sentiment analysis tool (https://doi.org/10.3390/app10175993)

### Requirements
-  docker
-  docker-compose


#### Development

The following command

```sh
$ docker-compose up -d --build
```

will build the images and run the containers. If you go to [http://localhost:5000](http://localhost:5000) you will see a web interface where you can check and test your REST API.

#### Production

The following command

```sh
$ docker-compose -f docker-compose.prod.yml up -d --build
```

will build the images and run the containers. The web interface is now available through nginx server at [http://localhost](http://localhost).

This tool can be freely used according to the MIT license. If you use this tool, please give the authors contribution by citing the following article  
 @article{pelicon2020zero,
title={Zero-shot learning for cross-lingual news sentiment classification},
author={Pelicon, Andra{\v{z}} and Pranji{'c}, Marko and Miljkovi{'c}, Dragana and {\v{S}}krlj, Bla{\v{z}} and Pollak, Senja},
journal={Applied Sciences},
volume={10},
number={17},
pages={5993},
year={2020},
publisher={Multidisciplinary Digital Publishing Institute}
}
