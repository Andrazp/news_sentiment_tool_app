from .src.data_transformation import prepare_data_for_prediction
from .src.MLBertModelForClassification import BertClassificationTraining


def predict(data, model, tokenizer, device, max_len=512, batch_size=32):
    """Runs the data thorugh the news sentiment model and returns model's predictions.
    The predictions are encoded as follows: 0 - negative; 1 - neutral; 2 - positive
    :param data: (list) list of input text documents
    :param model: (object) trained model
    :param tokenizer: (object) classifier tokenizer
    :param device: (object) device on which to run the model for inference, either gpu or cpu
    :param max_len: (int) maximum length of input sequence
    :param batch_size: (int) batch size
    :return (list) model predictions
    """
    bert_trainer = BertClassificationTraining(model, device)

    dataloader = prepare_data_for_prediction(data, tokenizer, max_len, batch_size)
    predictions = bert_trainer.predict(dataloader)
    return predictions