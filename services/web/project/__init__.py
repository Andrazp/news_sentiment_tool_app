import os
import json

from flask import (
    Flask,
    jsonify,
    send_from_directory,
    request,
    redirect,
    url_for
)

from flask_restx import Api, Resource, fields, abort, reqparse
import werkzeug
werkzeug.cached_property = werkzeug.utils.cached_property
from werkzeug.utils import secure_filename
from werkzeug.middleware.proxy_fix import ProxyFix

import torch
from transformers import BertTokenizer, BertConfig, BertForSequenceClassification

from . import api_functions


app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0',
          title='API services',
          description='Cross-lingual News Sentiment Classifier REST API')
ns = api.namespace('rest_api', description='REST services API')

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
tokenizer = BertTokenizer.from_pretrained('project/model', do_lower_case=False)

config = BertConfig.from_pretrained('project/model')
model = BertForSequenceClassification.from_pretrained('project/model',
                                                          config=config)


# input and output definitions
classifier_input = api.model('ClassifierInput', {
    'text': fields.List(fields.String, required=True, description='Title + body of the article'),
})

classifier_output = api.model('ClassifierOutput', {
    'sentiment': fields.List(fields.String, description='Sentiment')
})


@ns.route('/analyze_sentiment/')
class SentimentClassifier(Resource):
    @ns.doc('Analyzes sentiment of a news article')
    @ns.expect(classifier_input, validate=True)
    @ns.marshal_with(classifier_output)
    def post(self):
        encoded_predictions = api_functions.predict(api.payload['text'], model, tokenizer, device)
        predictions = []
        for p in encoded_predictions:
            if p == 0:
                predictions.append("negative")
            elif p == 1:
                predictions.append("neutral")
            elif p == 2:
                predictions.append("positive")
        return {"sentiment": predictions}


@ns.route('/health/')
class Health(Resource):
    @ns.response(200, "successfully fetched health details")
    def get(self):
        return {"status": "running", "message": "Health check successful"}, 200, {}
